/*
 * readelf.c
 *
 * Simple ELF parser
 *
 * Copyright (C) Bradley Gamble 2018
 */

#include <stdio.h>
#include <elf.h>

#include "arm-bare.h"

static int read_elf32_program(const Elf32_Ehdr* hdr32);
static int read_elf64_program(const Elf64_Ehdr* hdr64);

static int read_elf32_section(const Elf32_Ehdr* hdr32);
static int read_elf64_section(const Elf64_Ehdr* hdr64);

/* Verify the ELF header */
static int read_elf_magic(char* elf_magic)
{
  int i = 0;
  while(i < 4)
  {
      if(elf_magic[i] != ELFMAG[i])
      {
          printf("Error in ELF header!\r\n");
          return 1;
      }
      i++;
  }
  return 0;
}

static int read_elf_header(char* e_ident)
{
  Elf32_Ehdr* hdr32;
  Elf64_Ehdr* hdr64;
  int is64bit = 0;

  /* Make sure this is the proper ELF file type */
  read_elf_magic(e_ident);

  printf("%-16s\t\t", "Magic:");
  for(int i = 0; i < 4; i++)
  {
    printf("%02x ", e_ident[i]);
  }
  printf("\r\n");

  /* Bit size */
  printf("%-16s\t\t", "Class:");
  switch(e_ident[EI_CLASS])
  {
    case ELFCLASS32 : printf("32-bit");        is64bit = 0;break;
    case ELFCLASS64 : printf("64-bit");        is64bit = 1;break;
    default         : printf("Unknown bits!");             break;
  }
  printf("\r\n");

  /* Endianess*/
  printf("%-16s\t\t", "Endian:");
  switch(e_ident[EI_DATA])
  {
    case ELFDATA2LSB : printf("Little-endian");   break;
    case ELFDATA2MSB : printf("Big-endian");      break;
    default          : printf("Unknown endian!"); break;
  }
  printf("\r\n");

  printf("%-16s\t\t", "Version:");
  printf("%d", e_ident[EI_VERSION]);
  printf("\r\n");

  /* Machine type */
  printf("%-16s\t\t", "OS ABI:");
  switch(e_ident[EI_OSABI])
  {
    case ELFOSABI_SYSV      : printf("System V");          break;
    case ELFOSABI_HPUX      : printf("HP-UX");             break;
    case ELFOSABI_NETBSD    : printf("NetBSD");            break;
    case ELFOSABI_GNU       : printf("Linux");             break;
    case ELFOSABI_SOLARIS   : printf("Sun Solaris");       break;
    case ELFOSABI_AIX       : printf("IBM AIX");           break;
    case ELFOSABI_IRIX      : printf("SGI Irix");          break;
    case ELFOSABI_FREEBSD   : printf("FreeBSD ");          break;
    case ELFOSABI_TRU64     : printf("Compax TRU64 UNIX"); break;
    case ELFOSABI_MODESTO   : printf("Novell Modesto");    break;
    case ELFOSABI_OPENBSD   : printf("OpenBSD");           break;
    case ELFOSABI_ARM_AEABI : printf("Arm EABI");          break;
    case ELFOSABI_ARM       : printf("ARM");               break;
    default                 : printf("Unknown ABI!");      break;
  }
  printf("\r\n");

  printf("%-16s\t\t", "ABI Version:");
  printf("%d", e_ident[EI_ABIVERSION]);
  printf("\r\n");

  if(is64bit == 0)
    {
      hdr32 = (Elf32_Ehdr*) e_ident;
      /* ELF type */
      printf("%-16s\t\t", "Type:");
      switch(hdr32->e_type)
      {
        case ET_REL  : printf("Relocatable");   break;
        case ET_EXEC : printf("Executable");    break;
        case ET_DYN  : printf("Shared");        break;
        case ET_CORE : printf("Core");          break;
        default      : printf("Unknown type!"); break;
      }
      printf("\r\n");

      /* Machine type */
      printf("%-16s\t\t", "Machine:");
      switch(hdr32->e_machine)
      {
        case EM_386      : printf("Intel 80386");          break;
        case EM_68K      : printf("Motorola 68000");       break;
        case EM_88K      : printf("Motorola 88000");       break;
        case EM_ARM      : printf("ARM");                  break;
        case EM_IA_64    : printf("Intel Itanium 64-bit"); break;
        case EM_X86_64   : printf("Intel x86-64");         break;
        case EM_AARCH64  : printf("ARM, 64-bit");          break;
        case EM_RISCV    : printf("RISC-V");               break;
        default          : printf("Unknown machine!");     break;
      }
      printf("\r\n");

      printf("%-16s\t\t", "Version:");
      printf("%d", hdr32->e_version);
      printf("\r\n");

      printf("%-16s\t\t", "Entry point:");
      printf("%d", hdr32->e_entry);
      printf("\r\n");

      read_elf32_program(hdr32);
      read_elf32_section(hdr32);
  }
  else
  {
    hdr64 = (Elf64_Ehdr*) e_ident;
    /* ELF type */
    printf("%-16s\t\t", "Type:");
    switch(hdr64->e_type)
    {
      case ET_REL  : printf("Relocatable");   break;
      case ET_EXEC : printf("Executable");    break;
      case ET_DYN  : printf("Shared");        break;
      case ET_CORE : printf("Core");          break;
      default      : printf("Unknown type!"); break;
    }
    printf("\r\n");

    /* Machine type */
    printf("%-16s\t\t", "Machine:");
    switch(hdr64->e_machine)
    {
      case EM_386      : printf("Intel 80386");          break;
      case EM_68K      : printf("Motorola 68000");       break;
      case EM_88K      : printf("Motorola 88000");       break;
      case EM_ARM      : printf("ARM");                  break;
      case EM_IA_64    : printf("Intel Itanium 64-bit"); break;
      case EM_X86_64   : printf("Intel x86-64");         break;
      case EM_AARCH64  : printf("ARM, 64-bit");          break;
      case EM_RISCV    : printf("RISC-V");               break;
      default          : printf("Unknown machine!");     break;
    }
    printf("\r\n");

    printf("%-16s\t\t", "Version:");
    printf("%d", hdr64->e_version);
    printf("\r\n");

    printf("%-16s\t\t", "Entry point:");
    printf("%ld", hdr64->e_entry);
    printf("\r\n");

    read_elf64_program(hdr64);
    read_elf64_section(hdr64);
}

  return(0);
}

static int read_elf32_program(const Elf32_Ehdr* hdr32)
{
  Elf32_Phdr* phdr;

  if(hdr32 == NULL)
  {
    return -1;
  }

  printf("-------------------\r\n");
  printf("PROGRAM HEADERS\r\n");
  printf("-------------------\r\n");

  printf("Type\t"
         "offset\t\t"
         "vaddr\t\t"
         "paddr\t\t"
         "filesz\t\t"
         "memsz\t\t"
         "flags\t"
         "align\r\n");

  for(int i = 0; i < hdr32->e_phnum; i++)
  {
    phdr = (Elf32_Phdr*) (hdr32->e_ident +
      (hdr32->e_phoff + (i * sizeof(Elf32_Phdr))));

    switch(phdr->p_type)
    {
      case PT_LOAD    : printf("Load");            break;
      case PT_DYNAMIC : printf("Dynamic");         break;
      case PT_INTERP  : printf("Interp");          break;
      case PT_NOTE    : printf("Note");            break;
      case PT_SHLIB   : printf("Shlib");           break;
      case PT_PHDR    : printf("Phdr");            break;
      default         : printf("???");             break;
    }
    printf("\t");

    printf("0x%08x\t", phdr->p_offset);
    printf("0x%08x\t", phdr->p_vaddr);
    printf("0x%08x\t", phdr->p_paddr);
    printf("0x%08x\t", phdr->p_filesz);
    printf("0x%08x\t", phdr->p_memsz);

    printf("%s", phdr->p_flags & PF_X ? "E":"");
    printf("%s", phdr->p_flags & PF_W ? "W":"");
    printf("%s", phdr->p_flags & PF_R ? "R":"");
    printf("\t");
    printf("%x\t", phdr->p_align);
    printf("\r\n");
  }
  return 0;
}


static int read_elf64_program(const Elf64_Ehdr* hdr64)
{
  Elf64_Phdr* phdr;

  if(hdr64 == NULL)
  {
    return -1;
  }

  printf("-------------------\r\n");
  printf("PROGRAM HEADERS\r\n");
  printf("-------------------\r\n");

  printf("Type\t"
         "offset\t\t"
         "vaddr\t\t"
         "paddr\t\t"
         "filesz\t\t"
         "memsz\t\t"
         "flags\t"
         "align\r\n");
  for(int i = 0; i < hdr64->e_phnum; i++)
  {
    phdr = (Elf64_Phdr*) (hdr64->e_ident +
      (hdr64->e_phoff + (i * sizeof(Elf64_Phdr))));

    switch(phdr->p_type)
    {
      case PT_LOAD    : printf("Load");            break;
      case PT_DYNAMIC : printf("Dynamic");         break;
      case PT_INTERP  : printf("Interp");          break;
      case PT_NOTE    : printf("Note");            break;
      case PT_SHLIB   : printf("Shlib");           break;
      case PT_PHDR    : printf("Phdr");            break;
      default         : printf("???");             break;
    }
    printf("\t");

    printf("0x%08lx\t", phdr->p_offset);
    printf("0x%08lx\t", phdr->p_vaddr);
    printf("0x%08lx\t", phdr->p_paddr);
    printf("0x%08lx\t", phdr->p_filesz);
    printf("0x%08lx\t", phdr->p_memsz);

    printf("%s", phdr->p_flags & ~PF_X ? "E":"");
    printf("%s", phdr->p_flags & ~PF_W ? "W":"");
    printf("%s", phdr->p_flags & ~PF_R ? "R":"");
    printf("\t");
    printf("%lx\t", phdr->p_align);
    printf("\r\n");
  }
  return 0;
}

static int read_elf32_section(const Elf32_Ehdr* hdr32)
{
  Elf32_Shdr* shdr;

  if(hdr32 == NULL)
  {
    return -1;
  }

  printf("-------------------\r\n");
  printf("SECTION HEADERS\r\n");
  printf("-------------------\r\n");

  printf("Name\t\t"
         "type\t\t"
         "flags\t"
         "addr\t\t"
         "offset\t\t"
         "size\t\t"
         "link\t\t"
         "info\t\t"
         "addralign\t"
         "entsize\r\n");
  for(int i = 0; i < hdr32->e_shnum; i++)
  {
    shdr = (Elf32_Shdr*) (hdr32->e_ident +
      (hdr32->e_shoff + (i * sizeof(Elf32_Shdr))));

    printf("%d\t\t", shdr->sh_name);

    switch(shdr->sh_type)
    {
      case SHT_NULL     : printf("NULL");     break;
      case SHT_PROGBITS : printf("PROGBIT");  break;
      case SHT_SYMTAB   : printf("SYMTAB");   break;
      case SHT_STRTAB   : printf("STRTAB");   break;
      case SHT_RELA     : printf("RELA");     break;
      case SHT_HASH     : printf("HASH");     break;
      case SHT_DYNAMIC  : printf("DYNAMIC");  break;
      case SHT_NOTE     : printf("NOTE");     break;
      case SHT_NOBITS   : printf("NOBITS");   break;
      case SHT_REL      : printf("REL");      break;
      case SHT_SHLIB    : printf("SHLIB");    break;
      case SHT_DYNSYM   : printf("DYNSYM");   break;
      case SHT_NUM      : printf("NUM");      break;
      default           : printf("???");      break;
    }
    printf("\t\t");

    printf("%s", shdr->sh_flags & SHF_WRITE            ? "W" : "");
    printf("%s", shdr->sh_flags & SHF_ALLOC            ? "A" : "");
    printf("%s", shdr->sh_flags & SHF_EXECINSTR        ? "X" : "");
    printf("%s", shdr->sh_flags & SHF_MERGE            ? "M" : "");
    printf("%s", shdr->sh_flags & SHF_STRINGS          ? "S" : "");
    printf("%s", shdr->sh_flags & SHF_INFO_LINK        ? "I" : "");
    printf("%s", shdr->sh_flags & SHF_LINK_ORDER       ? "L" : "");
    printf("%s", shdr->sh_flags & SHF_OS_NONCONFORMING ? "O" : "");
    printf("%s", shdr->sh_flags & SHF_GROUP            ? "G" : "");
    printf("%s", shdr->sh_flags & SHF_TLS              ? "T" : "");
    printf("%s", shdr->sh_flags & SHF_COMPRESSED       ? "C" : "");
    printf("\t");

    printf("0x%08d\t", shdr->sh_addr);
    printf("0x%08d\t", shdr->sh_offset);
    printf("0x%08d\t", shdr->sh_size);
    printf("0x%08d\t", shdr->sh_link);
    printf("0x%08d\t", shdr->sh_info);
    printf("0x%08d\t", shdr->sh_addralign);
    printf("0x%08d\t", shdr->sh_entsize);
    printf("\r\n");
  }
  return 0;
}

static int read_elf64_section(const Elf64_Ehdr* hdr64)
{
  Elf64_Shdr* shdr;

  if(hdr64 == NULL)
  {
    return -1;
  }

  printf("-------------------\r\n");
  printf("SECTION HEADERS\r\n");
  printf("-------------------\r\n");

  printf("Name\t\t"
         "type\t\t"
         "flags\t"
         "addr\t\t"
         "offset\t\t"
         "size\t\t"
         "link\t\t"
         "info\t\t"
         "addralign\t"
         "entsize\r\n");
  for(int i = 0; i < hdr64->e_shnum; i++)
  {
    shdr = (Elf64_Shdr*) (hdr64->e_ident +
      (hdr64->e_shoff + (i * sizeof(Elf64_Shdr))));

    printf("%d\t\t", shdr->sh_name);

    switch(shdr->sh_type)
    {
      case SHT_NULL     : printf("NULL");     break;
      case SHT_PROGBITS : printf("PROGBIT");  break;
      case SHT_SYMTAB   : printf("SYMTAB");   break;
      case SHT_STRTAB   : printf("STRTAB");   break;
      case SHT_RELA     : printf("RELA");     break;
      case SHT_HASH     : printf("HASH");     break;
      case SHT_DYNAMIC  : printf("DYNAMIC");  break;
      case SHT_NOTE     : printf("NOTE");     break;
      case SHT_NOBITS   : printf("NOBITS");   break;
      case SHT_REL      : printf("REL");      break;
      case SHT_SHLIB    : printf("SHLIB");    break;
      case SHT_DYNSYM   : printf("DYNSYM");   break;
      case SHT_NUM      : printf("NUM");      break;
      default           : printf("???");      break;
    }
    printf("\t\t");

    printf("%s", shdr->sh_flags & SHF_WRITE            ? "W" : "");
    printf("%s", shdr->sh_flags & SHF_ALLOC            ? "A" : "");
    printf("%s", shdr->sh_flags & SHF_EXECINSTR        ? "X" : "");
    printf("%s", shdr->sh_flags & SHF_MERGE            ? "M" : "");
    printf("%s", shdr->sh_flags & SHF_STRINGS          ? "S" : "");
    printf("%s", shdr->sh_flags & SHF_INFO_LINK        ? "I" : "");
    printf("%s", shdr->sh_flags & SHF_LINK_ORDER       ? "L" : "");
    printf("%s", shdr->sh_flags & SHF_OS_NONCONFORMING ? "O" : "");
    printf("%s", shdr->sh_flags & SHF_GROUP            ? "G" : "");
    printf("%s", shdr->sh_flags & SHF_TLS              ? "T" : "");
    printf("%s", shdr->sh_flags & SHF_COMPRESSED       ? "C" : "");
    printf("\t");

    printf("0x%08ld\t", shdr->sh_addr);
    printf("0x%08ld\t", shdr->sh_offset);
    printf("0x%08ld\t", shdr->sh_size);
    printf("0x%08d\t", shdr->sh_link);
    printf("0x%08d\t", shdr->sh_info);
    printf("0x%08ld\t", shdr->sh_addralign);
    printf("0x%08ld\t", shdr->sh_entsize);
    printf("\r\n");
  }
  return 0;
}


int main(int argc, char* argv[])
{
  int errno = 0;

  /* Read the ELF header */
  if(read_elf_header((char*)arm_bare_elf))
  {
    printf("Failed to read elf header.\r\n");
    errno = -1;
  }

  return(errno);
}
