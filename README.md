# readelf-bare

A lightweight ELF parser designed for bare-metal systems.

This application has been developed in reference to the Tool Interface Standard 
Portable Formats Specification Version 1.1.
